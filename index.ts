import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetEditComponent } from './src/base-widget.component';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
  	MaterialModule,
    BrowserAnimationsModule
  ],
  declarations: [
	BaseWidgetEditComponent 
  ],
  exports: [
	BaseWidgetEditComponent 
  ]
})
export class EditModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EditModule,
      providers: []
    };
  }
}
